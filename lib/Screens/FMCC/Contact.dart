import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/cupertino.dart';
import 'package:fmccirafeed/SizeConfig.dart';
import 'package:flutter_open_whatsapp/flutter_open_whatsapp.dart';
import 'dart:async';
import 'package:flutter/services.dart';
import 'package:fmccirafeed/Screens/FMCC/ResultPage.dart';

class Contact extends StatelessWidget {
  String _platformVersion = 'Unknown';


  @override
  Future<void> initPlatformState() async {
    String platformVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      platformVersion = await FlutterOpenWhatsapp.platformVersion;
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.

  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
            backgroundColor: Colors.white,
            body: Column(mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                      Container(
                      height: 200,
                      width: double.infinity,
                      decoration: BoxDecoration(image: DecorationImage(image: AssetImage('images/irafeedlogo.jpeg'),fit: BoxFit.contain)),),
                      Text('IRAFEED INDUSTRIES',style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal *7),),
                      Text('B4 Commercial Market, Vidhya Nagar,Bhopal',style: TextStyle(fontSize: SizeConfig.blockSizeHorizontal*4),),
                  GestureDetector(
                    onTap: (){
                      FlutterOpenWhatsapp.sendSingleMessage('917067744441', "Hello");
                    },
                    child: Container(
                      height: 100,
                      width :double.infinity,
                      decoration: BoxDecoration(image: DecorationImage(image: AssetImage('images/whatsapp.jpeg'),fit: BoxFit.contain,),),
        ),
        ),

  ],
),
);
  }
}


