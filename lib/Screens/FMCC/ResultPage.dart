import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:fmccirafeed/Utilities/Calculations.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import '../../SizeConfig.dart';
import 'Contact.dart';

class ResultPage extends StatelessWidget {
  double dryOnly;
  double dry;
  double green;
  double feedRequirement;
  double cost;
  String animal;
  int imagenumber;
  double milkproduction;
  ResultPage(
      {this.dryOnly,
      this.dry,
      this.green,
      this.feedRequirement,
      this.animal,
      this.cost,
      this.milkproduction,
      this.imagenumber});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: Stack(children: [
        Container(
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [Colors.white, Colors.deepOrange],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter),
          ),
        ),
        Container(
          height: double.infinity,
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                height: MediaQuery.of(context).size.height / 3,
                child: Stack(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(bottom: 20),
                      child: Container(
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage('images/wave.png'),
                                fit: BoxFit.cover)),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage('images/wave.png'),
                              fit: BoxFit.cover)),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
        SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: [
                      Container(
                        width: double.infinity,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: Text(
                                '_dry',
                                style: TextStyle(
                                    color: Colors.pink,
                                    fontSize:
                                        SizeConfig.blockSizeHorizontal * 4,
                                    fontWeight: FontWeight.bold),
                              ).tr(),
                            ),
                            Text(
                              "${dryOnly.toStringAsFixed(2)} Kgs",
                              style: TextStyle(
                                  color: Colors.pink,
                                  fontSize: SizeConfig.blockSizeHorizontal * 6,
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: Text(
                          "_or",
                          style: TextStyle(
                              color: Colors.pink,
                              fontSize: SizeConfig.blockSizeHorizontal * 4,
                              fontWeight: FontWeight.bold),
                        ).tr(),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(1.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(4.0),
                                    child: Text(
                                      '_dry',
                                      style: TextStyle(
                                          color: Colors.pink,
                                          fontSize:
                                              SizeConfig.blockSizeHorizontal *
                                                  4,
                                          fontWeight: FontWeight.bold),
                                    ).tr(),
                                  ),
                                  Text(
                                    "${dry.toStringAsFixed(2)} Kgs",
                                    style: TextStyle(
                                        color: Colors.pink,
                                        fontSize:
                                            SizeConfig.blockSizeHorizontal * 6,
                                        fontWeight: FontWeight.bold),
                                  )
                                ],
                              ),
                            ),
                            Text(
                              '+',
                              style: TextStyle(color: Colors.pink),
                            ),
                            Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(4.0),
                                    child: Text(
                                      '_green',
                                      style: TextStyle(
                                          color: Colors.green.shade700,
                                          fontSize:
                                              SizeConfig.blockSizeHorizontal *
                                                  4,
                                          fontWeight: FontWeight.bold),
                                    ).tr(),
                                  ),
                                  Text(
                                    "${green.toStringAsFixed(2)} Kgs",
                                    style: TextStyle(
                                        color: Colors.pink,
                                        fontSize:
                                            SizeConfig.blockSizeHorizontal * 6,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              '_feedmeal',
                              style: TextStyle(
                                  fontSize:
                                      SizeConfig.blockSizeHorizontal * 4.5,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.pink),
                            ).tr(),
                            Text(
                              '${feedRequirement.toStringAsFixed(2)} Kgs',
                              style: TextStyle(
                                  fontSize: SizeConfig.blockSizeHorizontal * 6,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.pink),
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Container(
                          width: MediaQuery.of(context).size.width / 2,
                          height: 400,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage(
                                      'images/Samriddhi$imagenumber.png'),
                                  fit: BoxFit.contain)),
                        ),
                      ),
                      Text(
                        '_cost',
                        style: TextStyle(
                            fontSize: SizeConfig.blockSizeHorizontal * 4.5,
                            color: Colors.pink,
                            fontWeight: FontWeight.bold),
                      ).tr(),
                      Text(
                        'RS. ${cost.toStringAsFixed(2)}*',
                        style: TextStyle(
                            fontSize: SizeConfig.blockSizeHorizontal * 6,
                            fontWeight: FontWeight.bold,
                            color: Colors.pink),
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Container(
                            width: double.infinity,
                            decoration: BoxDecoration(
                              color: Colors.pink,
                              borderRadius: BorderRadius.circular(30),
                            ),
                            child: Center(
                              child: Padding(
                                padding: const EdgeInsets.all(15.0),
                                child: Text(
                                  "_recalculate",
                                  style: TextStyle(color: Colors.white),
                                ).tr(),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: GestureDetector(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) {
                                return Contact();
                              }),
                            );
                          },
                          child: Container(
                            width: double.infinity,
                            decoration: BoxDecoration(
                              color: Colors.pink,
                              borderRadius: BorderRadius.circular(30),
                            ),
                            child: Center(
                              child: Padding(
                                padding: const EdgeInsets.all(15.0),
                                child: Text(
                                  "_shop",
                                  style: TextStyle(color: Colors.white),
                                ).tr(),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ]),
    );
  }
}
