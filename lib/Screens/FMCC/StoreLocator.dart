import 'dart:async' show Future;
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fmccirafeed/SizeConfig.dart';
import 'package:fmccirafeed/Utilities/DistanceCal.dart';
import 'package:fmccirafeed/Utilities/Location.dart';
import 'package:google_map_polyline/google_map_polyline.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'dart:convert';
import 'package:flutter/services.dart' show rootBundle;
import 'package:fmccirafeed/Utilities/Location.dart';
import 'package:permission/permission.dart';
import 'package:fmccirafeed/Store/Navigation.dart';

class StoreLocator extends StatefulWidget {
  @override
  _StoreLocatorState createState() => _StoreLocatorState();
}

class _StoreLocatorState extends State<StoreLocator> {
  var lat, lng;
  List data;
  List<Location> locations = [
    Location(latitude: 24.1540016),
    Location(longitude: 72.4370951),
    Location(latitude: 24.141982),
    Location(longitude: 72.479001),
    Location(latitude: 24.6375222),
    Location(longitude: 71.895443),
  ];

  final Set<Polyline> polyline = {};
  GoogleMapController _controller;
  List<LatLng> routeCoords = [];
  GoogleMapPolyline googleMapPolyline =
      new GoogleMapPolyline(apiKey: "AIzaSyCl9Nlf2JeVqVlJtrk85zJDgGLyqJ9j0f8");
  BitmapDescriptor pinLocationIcon;
  Set<Marker> _markers = {};

  getSomePoints() async {
    var permissions =
        await Permission.getPermissionsStatus([PermissionName.Location]);
    if (permissions[0].permissionStatus == PermissionStatus.notAgain) {
      var askpermissions =
          await Permission.requestPermissions([PermissionName.Location]);
    } else {
      routeCoords = await googleMapPolyline.getCoordinatesWithLocation(
          origin: LatLng(24.1419821, 72.4790007),
          destination: LatLng(24.1540016, 72.4370951),
          mode: RouteMode.driving);
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getUserLocation();
    getSomePoints();
    BitmapDescriptor.fromAssetImage(
            ImageConfiguration(devicePixelRatio: 2.5), 'images/marker.png')
        .then((value) => pinLocationIcon);
  }

  /* static void launchMapsUrl(
      sourceLatitude,
      sourceLongitude,
      destinationLatitude,
      destinationLongitude) async {
    String mapOptions = [
      'saddr=$sourceLatitude,$sourceLongitude',
      'daddr=$destinationLatitude,$destinationLongitude',
      'dir_action=navigate'
    ].join('&');

    final url = 'https://www.google.com/maps?$mapOptions';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }  }*/

  Future getUserLocation() async {
    var position = await GeolocatorPlatform.instance
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);

    setState(() {
      lat = position.latitude;
      lng = position.longitude;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Store Locator'),
      ),
      body: Stack(children: [
        if (lat == null || lng == null)
          Container()
        else
          Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                height: 300,
                width: double.infinity,
                child: GoogleMap(
                  onMapCreated: onMapCreated,
                  polylines: polyline,
                  initialCameraPosition: CameraPosition(
                    target: LatLng(lat, lng),
                    zoom: 16,
                  ),
                  myLocationEnabled: true,
                  myLocationButtonEnabled: true,
                  mapType: MapType.normal,
                  zoomGesturesEnabled: true,
                  zoomControlsEnabled: true,
                ),
              ),
            ),
            RaisedButton(
                child: Text('Find a Store'),
                onPressed: () {
                  showDialog(
                      context: context,
                      builder: (context) {
                        return Dialog(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20.0),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                              child: ListView.builder(
                                itemBuilder: (BuildContext context, int index) {
                                  return GestureDetector(
                                    onTap: () {
                                      Navigator.pop(context);
                                      setState(() {
                                        _controller.animateCamera(
                                          CameraUpdate.newCameraPosition(
                                            CameraPosition(
                                              zoom: 16,
                                            ),
                                          ),
                                        );
                                      });
                                    },
                                    child: new Card(),
                                  );
                                },
                              ),
                            ),
                          ),
                        );
                      });
                }),
            RaisedButton(
              child: Text('Find the Closest Store'),
              onPressed: () {},
            )
          ]),
      ]),
    );
  }

  void onMapCreated(GoogleMapController controller) {
    setState(() {
      _controller = controller;

      _markers.add(Marker(
        markerId: MarkerId('<MARKER_ID'),
        position: LatLng(lat, lng),
        icon: pinLocationIcon,
      ));

      polyline.add(Polyline(
        polylineId: PolylineId('route1'),
        visible: true,
        points: routeCoords,
        width: 10,
        color: Colors.blue,
        startCap: Cap.roundCap,
        endCap: Cap.buttCap,
      ));
    });
  }
}
