import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fmccirafeed/Screens/FMCC/ResultPage.dart';
import 'package:fmccirafeed/SizeConfig.dart';
import 'package:fmccirafeed/Utilities/ScreenOrientation.dart';
import 'package:fmccirafeed/Utilities/Calculations.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

enum Animal {
  cow,
  buffalo,
}

class CostCalculator extends StatefulWidget {
  static const String id = "Cost Calculator";

  @override
  _CostCalculatorState createState() => _CostCalculatorState();
}

class _CostCalculatorState extends State<CostCalculator> {
  Animal animal = Animal.cow;

  double numberofanimals = 1;
  double weightPerAnimal = 500;
  double totalMilkProduction = 10;
  double dryvalue;
  double greenvalue;
  double feedreq;
  double totalcost = 0;
  double labour = 30;
  double newdryvalue;
  double reqperlitremilk;
  int a;

  @override
  void initState() {
    // TODO: implement initState
    setScreenOrientation();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      bottomSheet: GestureDetector(
        onTap: () {
          dryvalue = calculateDryOnly(weightPerAnimal);
          print(dryvalue);
          greenvalue = calculateGreen(weightPerAnimal);
          newdryvalue = dryvalue - (greenvalue / 10);
          a = feedimage(totalMilkProduction, animal.toString());
          feedreq = calculateFeedRequirement(
              totalMilkProduction, weightPerAnimal, animal.toString());
          print(feedreq);
          reqperlitremilk = totalMilkProduction * 0.4;
          print(reqperlitremilk);
          if (animal == Animal.cow) {
            if (totalMilkProduction <= 15) {
              totalcost = (dryvalue * 6 +
                      (feedreq - totalMilkProduction * 0.4) * 25 +
                      reqperlitremilk * 25 +
                      labour) /
                  totalMilkProduction;
            } else {
              totalcost = (dryvalue * 6 +
                      (feedreq - totalMilkProduction * 0.3) * 25 +
                      reqperlitremilk * 25 +
                      labour) /
                  totalMilkProduction;
            }
          } else {
            totalcost = (dryvalue * 6 +
                    (feedreq - totalMilkProduction * 0.4) * 30 +
                    reqperlitremilk * 30 +
                    labour) /
                totalMilkProduction;
          }
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) {
              return ResultPage(
                  dryOnly: dryvalue,
                  dry: newdryvalue,
                  green: greenvalue,
                  feedRequirement: feedreq,
                  cost: totalcost,
                  milkproduction: totalMilkProduction,
                  imagenumber: a);
            }),
          );
        },
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            height: 60,
            width: double.infinity,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.pink,
            ),
            child: Center(
              child: Text(
                '_calculate',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: SizeConfig.safeBlockHorizontal * 4),
              ).tr(),
            ),
          ),
        ),
      ),
      body: Stack(
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [Colors.deepOrange, Colors.pink],
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter),
            ),
          ),
          Container(
            height: double.infinity,
            width: double.infinity,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Container(
                  height: MediaQuery.of(context).size.height / 3,
                  child: Stack(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(bottom: 20),
                        child: Container(
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage('images/wave.png'),
                                  fit: BoxFit.cover)),
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage('images/wave.png'),
                                fit: BoxFit.cover)),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
          SafeArea(
            child: Container(
              child: Column(
                children: [
                  SizedBox(
                    height: 30,
                  ),
                  Text(
                    'ani',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: SizeConfig.safeBlockHorizontal * 4),
                  ).tr(),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(15, 30, 15, 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              animal = Animal.cow;
                            });

                            print(animal);
                          },
                          child: Container(
                            width: MediaQuery.of(context).size.width / 3,
                            decoration: BoxDecoration(
                                color: animal != Animal.cow
                                    ? Colors.white.withOpacity(0.5)
                                    : Colors.white,
                                borderRadius: BorderRadius.circular(30)),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 20, vertical: 5),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(
                                    Icons.play_arrow,
                                    color: Colors.deepOrange,
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Text(
                                    '_cow',
                                    style: TextStyle(
                                        color: Colors.deepOrange,
                                        fontSize:
                                            SizeConfig.safeBlockHorizontal *
                                                3.6),
                                  ).tr(),
                                ],
                              ),
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              animal = Animal.buffalo;
                            });
                            print(animal.toString());
                          },
                          child: Container(
                            width: MediaQuery.of(context).size.width / 3,
                            decoration: BoxDecoration(
                                color: animal != Animal.cow
                                    ? Colors.white
                                    : Colors.white.withOpacity(0.5),
                                borderRadius: BorderRadius.circular(30)),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 20, vertical: 5),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(
                                    Icons.play_arrow,
                                    color: Colors.deepOrange,
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Text(
                                    '_buffalo',
                                    style: TextStyle(
                                        color: Colors.deepOrange,
                                        fontSize:
                                            SizeConfig.safeBlockHorizontal *
                                                3.6),
                                  ).tr(),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 20, horizontal: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          '_numberofanimals',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: SizeConfig.safeBlockHorizontal * 4),
                        ).tr(),
                        Text(
                          '${numberofanimals.toString().split('.')[0]}',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: SizeConfig.safeBlockHorizontal * 6),
                        ),
                      ],
                    ),
                  ),
                  Slider(
                    value: numberofanimals,
                    min: 0,
                    max: 200,
                    activeColor: Colors.white,
                    onChanged: (value) {
                      setState(() {
                        numberofanimals = value;
                      });
                    },
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 20, horizontal: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          '_averageweightperanimal',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: SizeConfig.safeBlockHorizontal * 4),
                        ).tr(),
                        Text(
                          '${weightPerAnimal.toString().split('.')[0]}',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: SizeConfig.safeBlockHorizontal * 6),
                        ),
                      ],
                    ),
                  ),
                  Slider(
                    value: weightPerAnimal,
                    min: 180,
                    max: 800,
                    activeColor: Colors.white,
                    onChanged: (value) {
                      setState(() {
                        weightPerAnimal = value;
                      });
                    },
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 20, horizontal: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          '_averagemilkperanimal',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: SizeConfig.safeBlockHorizontal * 4),
                        ).tr(),
                        Text(
                          '${totalMilkProduction.toString().split('.')[0]}',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: SizeConfig.safeBlockHorizontal * 6),
                        ),
                      ],
                    ),
                  ),
                  Slider(
                    value: totalMilkProduction,
                    min: 0,
                    max: 40,
                    activeColor: Colors.white,
                    onChanged: (value) {
                      setState(() {
                        totalMilkProduction = value.roundToDouble();
                        print(totalMilkProduction);
                      });
                    },
                  ),
                  Expanded(
                    child: Padding(
                      padding:
                          const EdgeInsets.only(top: 12, left: 8, right: 8),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: GestureDetector(
                              onTap: () {
                                setState(() {
                                  context.locale = Locale('en', 'IN');
                                });
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                  color: Colors.pink,
                                ),
                                height: 50,
                                child: Center(
                                    child: Text(
                                  'English',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize:
                                          SizeConfig.blockSizeHorizontal * 4.5),
                                )),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                            width: 10,
                          ),
                          Expanded(
                            child: GestureDetector(
                              onTap: () {
                                setState(() {
                                  context.locale = Locale('hi', 'IN');
                                });
                              },
                              child: Container(
                                height: 50,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                  color: Colors.pink,
                                ),
                                child: Center(
                                    child: Text(
                                  'हिन्दी',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize:
                                          SizeConfig.blockSizeHorizontal * 4.5),
                                )),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                            width: 10,
                          ),
                          Expanded(
                            child: GestureDetector(
                              onTap: () {
                                setState(() {
                                  context.locale = Locale('gu', 'IN');
                                });
                              },
                              child: Container(
                                height: 50,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                  color: Colors.pink,
                                ),
                                child: Center(
                                    child: Text(
                                  'ગુજરાતી',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize:
                                          SizeConfig.blockSizeHorizontal * 4.5),
                                )),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
