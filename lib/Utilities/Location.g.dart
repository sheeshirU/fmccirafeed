// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Location.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

StoreData _$locationFromJson(Map<String, dynamic> json) {
  return StoreData(
    Lat: (json['Lat'] as num)?.toDouble(),
    Lng: (json['Lng'] as num)?.toDouble(),
    name: json['name'] as String,
    businessname: json['businessname'] as String,
    address: json['address'] as String,
    phone: json['phone'] as String,
  );
}

Map<String, dynamic> _$locationToJson(StoreData instance) => <String, dynamic>{
      'name': instance.name,
      'businessname': instance.businessname,
      'address': instance.address,
      'phone': instance.phone,
      'Lat': instance.Lat,
      'Lng': instance.Lng,
    };
