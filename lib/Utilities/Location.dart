import 'package:json_annotation/json_annotation.dart';

part 'Location.g.dart';
@JsonSerializable()

class StoreData {
  StoreData({this.Lat,this.Lng,this.name,this.businessname,this.address,this.phone, lat1});
  final String name;
  final String businessname;
  final String address;
  final String phone;
  final double Lat;
  final double Lng;



  factory StoreData.fromJson(Map<String,dynamic> json)=> _$locationFromJson(json);
  Map<String, dynamic> toJson() => _$locationToJson(this);


  }
