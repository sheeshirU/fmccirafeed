import 'package:flutter/material.dart';
import 'package:fmccirafeed/Utilities/Calculations.dart';

int a;

double calculateDryOnly(double weight) {
  return weight * 0.015;
}

double calculateGreen(double weight) {
  return weight * 0.015 * 10 * 0.3;
}

double calculateFeedRequirement(double milk, double weight, String animal) {
  if (animal == 'Animal.cow') {
    if (milk <= 15) {
      return weight * 0.003 + milk * 0.4;
    } else {
      return weight * 0.003 + milk * 0.3;
    }
  } else {
    return weight * 0.003 + milk * 0.4;
  }
}

int feedimage(double milk, String animal) {
  if (animal == 'Animal.cow') {
    if (milk <= 15) {
      return 2;
    } else {
      return 1;
    }
  } else {
    return 3;
  }
}
