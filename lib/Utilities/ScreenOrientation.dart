import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void setScreenOrientation() async {
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
}
