/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.ly.permission;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "com.ly.permission";
  public static final String BUILD_TYPE = "debug";
  public static final int VERSION_CODE = -1;
  public static final String VERSION_NAME = "";
}
